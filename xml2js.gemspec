
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "xml2js/version"

Gem::Specification.new do |spec|
  spec.name          = "xml2js"
  spec.version       = Xml2js::VERSION
  spec.authors       = ["Michael"]
  spec.email         = ["micharch54@gmail.com"]

  spec.summary       = %q{Wrapper for the excellent xml2js xml parser from node}
  spec.description   = %q{This will parse xml and all attributes into an easily traversable hash}
  spec.homepage      = "https://github.com/neener54/xml2js"
  spec.license       = "MIT"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
