let Parser = require('xml2js').Parser
let readFile = require('fs').readFile
let writeFile = require('fs').writeFile

let parseOpts = {
  trim: true,
  normalizeTags: true,
  normalize: true
}
let parser = new Parser(parseOpts)
let fileName = process.argv[2]
let outFile = process.argv[3]
let parseFile = (data) =>{
  parser.parseString(data, (parseErr, result) => {
    if(parseErr){
      throw parseErr
    }
    writeFile(outFile, JSON.stringify(result), (err) => {
      if (err) {
        throw err
      }
    })
  })
}

readFile(fileName, (err, data) => {
  if(err) {
    throw err
  }
  parseFile(data)
  return
})

