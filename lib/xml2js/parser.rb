require 'tempfile'
require 'json'
module Xml2js
  class Parser
    DEFAULT_SCRIPT = './lib/xml2js/parser.js'

    def self.parse(data, script=DEFAULT_SCRIPT)
      parser = new script
      parser.xml_to_hash data
    end
    def initialize(script)
      @parser = script
    end

    def xml_to_hash(data)
      tmp_dir = "#{File.expand_path(__dir__)}/tmp"
      file = Tempfile.new(['xml-data', '.xml'], tmp_dir)
      json_file = Tempfile.new(['json-data', '.json'], tmp_dir)
      begin
        file.write(data)
        file.rewind
        json_file.rewind
        %x{ node #{@parser} #{file.path} #{json_file.path}}
        JSON.parse(File.read(json_file.path), {symbolize_names: true})
      ensure
        file.close
        file.unlink
      end
    end
  end
end
