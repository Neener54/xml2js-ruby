# Xml2js

Xml2js is intended to be a wrapper around the excellent xml2js node package. While at my previous company we ran into many instances where current ruby xml parsers just simply did not capture everything in the long xml files that we used. The node version did. So to solve for this in our Ruby apps we can actually use Xml2js. 
## Installation

Add this line to your application's Gemfile:

```ruby
gem 'xml2js'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install xml2js

## Usage

```ruby
require 'xml2js'
xml_hash = Xml2js::Parser.parse(xml_data)

```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/Neener54/xml2js. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Xml2js project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/Neener54/xml2js/blob/master/CODE_OF_CONDUCT.md).
