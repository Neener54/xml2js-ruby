require 'spec_helper'
require 'xml2js'

RSpec.describe Xml2js::Parser do
  context 'translating xml documents' do
    it 'should translate the xml data to a hash' do

      file = File.read('spec/fixtures/menu.xml')

      output = Xml2js::Parser.parse(file)
      expect(output).to be_a(Hash)
      expect(output[:breakfast_menu][:food][0][:price][0]).to eq("$5.95")
    end

    it 'should throw an error when given bad xml' do
      file = File.read('spec/fixtures/index.html')
      expect{ Xml2js::Parser.parse(file) }.to raise_error(JSON::ParserError)
    end
  end
end
